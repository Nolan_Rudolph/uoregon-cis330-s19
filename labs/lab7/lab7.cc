/*************************************
CIS 330 Lab 7: Operator overloading, namespaces, and static members

The idea of this lab is to improve a simple queue data structure by adding overloaded operators for common tasks.
Specifically, you will:

  1) Overload the << operator so that objects of class IntQueue can be inserted into ostreams.
     For example, if Q is an object of class IntQueue, the expression `cout << Q` should print the contents of Q on standard out.

  2) Overload the << operator so that it can be used to enqueue ints.
     For example, if Q is an object of class IntQueue, the expression `Q << 10 << 20 << 30` should push 10, 20, and 30 onto the queue in that order.

  3) Overload the >> operator so that it can be used to dequeue ints.
     For example, if Q is an object of class IntQueue with some values already enqueued and a, b, and c are int variables,
     the expression `Q >> a >> b >> c` should dequeue the top three values from the queue and store them in a, b, and c.

Ruberic:

  Part 1:        5 points
  Parts 2 and 3: 5 points


Extra credit:

  Find and fix my memory leak: 1 point

*************************************/

#include <iostream>
//
// A namespace to hold the queue class and other related functions, variables, and constants.
//
namespace Mem {

  //
  // This class provides a generic linked-list element
  //
  template <class T>
  class Elem {
    T data;
    Elem *next;
    int id;

    //
    // This static data member is incremented each time an Elem object is constructed,
    // allowing the generation of a unique id for each Elem object.
    // Clearly this is overkill for this lab, but you can imagine where this might be useful.
    //
    static int count;

  public:
    //
    // Constructors
    //
    Elem() : next(NULL), id(count++) {}
    Elem(T newData) : data(newData), next(NULL), id(count++) {}
    Elem(T newData, Elem *newNext) : data(newData), next(newNext), id(count++) {}
    
    //
    // Allow access to `Next` data member through getter and setter
    //
    Elem * setNext(Elem *newNext) { next = newNext; return this; }
    Elem * getNext() { return next; }
    
    //
    // Allow access to `Data` data member through getter and setter
    //
    Elem * setData(T newData) { data = newData; return this; }
    T getData() { return data; }

    //
    // Accept overloaded << operator as friend
    //
    template <class J>
    friend std::ostream & operator<<(std::ostream &s, Elem<J> *e);
  };

  
  //
  // This class implements a queue of ints on a linked list of Elem objects
  //
  class IntQueue {

    Elem<int> *head;
    Elem<int> *tail;
		

  public:
    //
    // Constructor
    //
    IntQueue(): head(NULL), tail(NULL) {}

    //
    // Basic queue methods
    //
    void Enqueue(int newValue);
    int Dequeue();
	
	// PART 1
	friend std::ostream &
	operator<<(std::ostream &s, IntQueue &Q);

	// PART 2.1
	friend IntQueue &
	operator<<(IntQueue &Q, const int &e);

	// PART 2.2
	friend IntQueue &
	operator>>(IntQueue &Q, int &place);

	~IntQueue() { 
		std::cout << "Deleting IntQueue Object" << std::endl;
		delete[] head; 
		delete[] tail; 
	};

  };
  // PART 1
  std::ostream &
  operator<<(std::ostream &s, IntQueue &Q);

  // PART 2.1
  IntQueue &
  operator<<(IntQueue &Q, const int &e);

  // PART 2.2
  IntQueue &
  operator>>(IntQueue &Q, int &place);

} // Mem namespace closes here

//
// The static counter for the `Elem` class must be initialized outside of the class definition.
//
template <class T>
int Mem::Elem<T>::count = 0;

//
// Implementation of enqueue method
//
void
Mem::IntQueue::Enqueue(int newValue)
{
  Elem<int> *tmp = new Elem<int>(newValue);
  if (tail == NULL) {
    head = tail = tmp;
  } else {
    tail->setNext(tmp);
    tail = tmp;
  }
}

//
// Implementation of dequeue method
//
int
Mem::IntQueue::Dequeue()
{
  Elem<int> *tmp = head;
  if (head == NULL) {
    return -1;
  }
  head = head->getNext();
  if (head == NULL) {
    tail = NULL;
  }
  int int_temp = tmp -> getData(); // EXTRA CREDIT
  delete tmp;                      // EXTRA CREDIT
  return int_temp;
}

//
// Note that we overload << to take a pointer to an object of type Elem and follow the pointer to print the object's actual fields.
// This is because the methods that deal with Elem objects (Enqueue, Dequeue) deal with pointers:
// it is then useful to be able to directly insert an object into a stream just from its pointer.
// 
template <class T>
std::ostream &
Mem::operator<<(std::ostream &s, Mem::Elem<T> *e)
{
  if (e == NULL) {
    s << "EmptyElem()" << std::endl;
  } else {
    s << "Elem(id=" << e->id << ", data=" << e->data << ", next=" << static_cast<void*>(e->next) << ")" << std::endl;
  }
  return s;
}

// We left the mem namespace, thus we must reference elements inside Mem namespace via "Mem::..."
// PART 1: Allowing std::cout << IntQueue to print all elements in the quque
std::ostream &
Mem::operator<<(std::ostream &s, Mem::IntQueue &Q)
{
	Elem<int> curr = *Q.head;
	while (curr.getNext() != NULL){  // Loop through the array granted by IntQueue Q
		s << curr.getData() << "\n"; // Send to the output stream specefied (cout in this case)
		curr = *curr.getNext();      // Set curr to the next item in the list
	}
	s << curr.getData();  			 // We stopped when the next was NULL, thus one element is unaccounted for

	return s;						 // Return the stream for further output
}

// PART 2.1: Allowing IntQueue << int to enqueue new integers
Mem::IntQueue &
Mem::operator<<(Mem::IntQueue &Q, const int &e)
{
	Q.Enqueue(e);  // Simply queue the integer

	return Q;      // Return the queue in the case that Q << int << int occurs
}

// PART 2.2: Allowing IntQueue >> int * to dequeue IntQueue and hold such integers at int *
Mem::IntQueue &
Mem::operator>>(IntQueue &Q, int &place)
{
	int temp;
	temp = Q.Dequeue();  // Store dequeued integer in temp

	place = temp;        // Use the reference to assign place (or temp[i]) to temp

	return Q;
}
//
// Test program 
//
int
main()
{
  Mem::IntQueue Q;

  std::cout << "\nTesting PART 2.1 via 'Q << 1 << 2 << 3 \
<< 4 << 5'" << std::endl;

  Q << 1 << 2 << 3 << 4 << 5;   // Overloaded << operator for PART 2.1 

  std::cout << "Testing PART 1 functionality via 'std::cout << Q << std::endl'"\
<< std::endl; 
  std::cout << Q << std::endl;  // Overloaded << operator for PART 1

  std::cout << "\n";

  std::cout << "Initializing array temp[5]" << std::endl;
  int* temp = new int[5];
 
  std::cout << "Testing PART 2.2 via 'Q >> temp[0] >> temp[1] >> \
temp[2] >> temp[3] >> temp[4]'" << std::endl; 
  Q >> temp[0] >> temp[1] >> temp[2] >> temp[3] >> temp[4]; // Overloaded >> operator for PART 2.2

  std::cout << "temp[0] should be 1 // temp[0] is " << temp[0] << std::endl;
  std::cout << "temp[1] should be 2 // temp[1] is " << temp[1] << std::endl;
  std::cout << "temp[2] should be 3 // temp[2] is " << temp[2] << std::endl;
  std::cout << "temp[3] should be 4 // temp[3] is " << temp[3] << std::endl;
  std::cout << "temp[4] should be 5 // temp[4] is " << temp[4] << std::endl;

  std::cout << "Q should be empty now. Q.Dequeue() should return -1: " << Q.Dequeue() << "\n\n"; 

  delete[] temp;

  return 0;
}
