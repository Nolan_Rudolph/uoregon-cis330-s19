/*********************************************

CIS330: Lab 3

Implementation file for the error reporting system

*********************************************/

#include <ohno.h>
#include <stdlib.h>

#define BLUE "\033[1;34m"
#define YELLOW "\033[1;33m"
#define RED "\033[1;31m"

static struct ohno_state *state;

int ohno_init(FILE *where_to, const char *app_name)
{
    state = malloc(sizeof(struct ohno_state));
    state -> out = where_to;
    state -> name = (char *)app_name;
    state -> error_number = 0;

    return 0;
}

void ohno_free()
{   
    free(state);
}

void ohno(const char *message, ohno_severity_t severity)
{
    if (severity == 0) fprintf(state -> out, "\033[1;33mWARNING\033[0m: %s\n", message);
    else if (severity == 1) fprintf(state -> out, "\033[0;35mSERIOUS\033[0m: %s\n", message);
    else if (severity == 2) fprintf(state -> out, "\033[1;31mFATAL\033[0m: %s\n", message);
    else fprintf(state -> out, "MISSINGNO.: %s\n", message);
}
