#include "cipher.h"

#define UPPER_CASE(r) ((r) - ('a' - 'A'))

struct Cipher::CipherCheshire {
    string cipherText;
};

Cipher::Cipher()
{
    smile = new CipherCheshire;
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
}
Cipher::Cipher(string in)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
}

string Cipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;
}

string Cipher::decrypt(string enc)
{
    string retStr;
    string cap_alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "; // Alphabet used for lower
    string alph = "abcdefghijklmnopqrstuvwxyz ";     // Alphabet used for upper
    int index;
    int upper;
    
    cout << "Decrypting..." << endl;

    for (unsigned int i = 0; i < enc.size(); i++) {
        index = 0;
        upper = 0;
        if (enc[i] >= 'A' && enc[i] <= 'Z')
            upper = 1;
        
        if (upper) {
            // Locating where to look for letter in cap_alph
            while (UPPER_CASE(smile -> cipherText[index]) != enc[i])
                ++index;
        }
        else {
            // Locating where to look for letter in alph
            while (smile -> cipherText[index] != enc[i])
                ++index;
        }
        // Append the true letter to the return string
        if (upper) 
            retStr += UPPER_CASE(alph[index]);
        else
            retStr += alph[index];
    }
    
    cout << "Done" << endl;

    return retStr;
}



struct CaesarCipher::CaesarCipherCheshire : CipherCheshire {
     int rot;
};

CaesarCipher::CaesarCipher()
{
    smile = new CipherCheshire;  // CaesarCiper inherits CipherCheshire inherits Cipher
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz "; // Allows us to adopt smile
    CaesarSmile = new CaesarCipherCheshire;  // CaesarCipher has new member CaesarSmile
}

CaesarCipher::CaesarCipher(string in, int rot)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile -> rot = rot;  // In this constructor, we can now define rot
}

// EXTRA CREDIT (++myCCipher)
void CaesarCipher::operator++() {
    // 'this' is used because the object (itself) is the one being influenced
    this -> CaesarSmile -> rot += 1; // Increment rot from structure CaesarSmile
}

// EXTRA CREDIT (myCCipher++)
void CaesarCipher::operator++(int) {
    this -> CaesarSmile -> rot += 1;
}

// EXTRA CREDIT (--myCCipher)
void CaesarCipher::operator--() {
    this -> CaesarSmile -> rot -= 1; // Decrement rot from structure CaesarSmile
}

// EXTRA CREDIT (myCCipher--)
void CaesarCipher::operator--(int) {
    this -> CaesarSmile -> rot -= 1;
}

string CaesarCipher::encrypt(string raw)
{
    string retStr = "";
    int rot = CaesarSmile -> rot;
    
    cout << "Encrypting..." << endl;
    
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        
        if (pos + rot > 26) {
            pos += rot;  // Caesar shift influence on encryption
            pos %= 26;   // But if it surpasses the alphabet, we need to loop
            
            // The following section was implemented because I'd often have 0
            // and then subtract it, causing arithmetic shifts to create
            // some insanely large number. So now its either 25, or minus 1
            // to compensate for indexing beginning at 0
            if (pos == 0) {
                pos = 25;
            }
            else {
                pos -= 1;
            }
        }
        else
            pos += rot; // If not exceeding boundaries, just add the shift
                
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }

    cout << "Done" << endl;

    return retStr;

}

string CaesarCipher::decrypt(string enc)
{

    string retStr = "";
    string cap_alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    string alph = "abcdefghijklmnopqrstuvwxyz ";
    int index;
    int upper;
    int rot = CaesarSmile -> rot;
    
    cout << "Decrypting..." << endl;
    
    for (unsigned int i = 0; i < enc.size(); i++) {
        index = 0;
        upper = 0;
        if (enc[i] >= 'A' && enc[i] <= 'Z')
            upper = 1;
        
        // Alike last decrypt method, we need to locate legitimate indexes
        if (upper) {
            while (UPPER_CASE(smile -> cipherText[index]) != enc[i])
                ++index;
        }
        else {
            while (smile -> cipherText[index] != enc[i])
                ++index;
        }
        
        // The following section essentially reverses the rot shift given by
        // encrypt. Yes, something is flawed because I lose a's, but that's it!
        // #pleaseLetTheExtraCreditCompensateForThis
        index -= rot;
        if (index < 0) {  // What if the reverse shift makes it negative?
            while (index < 0)  // If we want Caeser shifts > 26, we need this
                index += 26;   // Add 26 to essentially make a full loop of alph
            index += 1;   // Again, indexing issue compensation
        }
        if (upper) 
            retStr += UPPER_CASE(alph[index]);
        else
            retStr += alph[index];
    }
     
    cout << "Done" << endl;

    return retStr;
}


