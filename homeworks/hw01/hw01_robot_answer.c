#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>


// Number of response our robot can make
#define NUM_RESPONSE 5

// Input the robot can recognize
// I made all entries lowercase to abide by my methodology.
// I could make a function for this but I thought it gratuitous when you
// could just as easily hard-code it. See my explanation in respond() for
// reasoning behind reordering of words.
char *INPUT_STR[] = {"age", 
                     "good", 
                     "sports", 
                     "leaving", 
                     "weather"};

// Corresponding response the robot should make
char *RESPONSE_STR[] = {"I am a robot and I am 4 hours old.", 
                        "Good day to you as well.", 
                        "Go Ducks!", 
                        "Good bye.", 
                        "It's raining.",
                        "I don't understand what you are saying."};


void arg_test(int argc, char **argv);
void print_response();
void respond(int argc, char **argv);
void proof_of_optimization(void);
void just_make_it_lower_case(int argc, char **argv);

int main(int argc, char **argv)
{
    // Give user a suggestive response if nothing is inputed
    arg_test(argc, argv);

    // Homework 1, Part 1
    respond(argc, argv);
    
    // proof_of_optimization();

    return 0;
}

void arg_test(int argc, char **argv)
{
    if(argc < 2) {
        fprintf(stderr, "Error: no input entered\n");
        fprintf(stderr, "usage: %s <some input string>\n", argv[0]);
        fprintf(stderr, "\n");
    } else {
        // Do nothing
    }

}

void proof_of_optimization(void) {
    int i, j, totalSum;
    for (i = 0; i < NUM_RESPONSE; ++i) {
        for (j = 0, totalSum = 0; j < strlen(INPUT_STR[i]); ++j) {
            totalSum += (int)INPUT_STR[i][j];
        }
        printf("Total Sum for %s is %d\n", INPUT_STR[i], totalSum);
        
    }
}

void just_make_it_lower_case(int argc, char **argv) {
    int i, j;
    for (i = 1; i < argc; ++i) {
        for (j = 0; j < strlen(argv[i]); ++j) {
            argv[i][j] = tolower(argv[i][j]);
        }
    }
}

void respond(int argc, char **argv)
{
    printf("---- Answer ----\n");
    
    int i, j, cmp;
    int is_found = 0;
    
    just_make_it_lower_case(argc, argv);
    
    for (i = 1; i < argc; ++i) {
        for (j = 0; j < NUM_RESPONSE; ++j) {
            /* The only way you could optimize this is by setting an
             * index of string weight via net ASCII summation, placing all words
             * in order where each successive word is a higher weight than the 
             * predecessor, and then breaking the loop once a known word's length 
             * is greater than a command line argument word. I created a
             * function, proof_of_optimization, to show how I forged the layout
             * of successive words within INPUT_STR[]. So if the comparison is
             * negative, AND the length of the argument word is less than or 
             * equal to the length of the comparison to the known word, there is 
             * no need to check the rest of the entries. 
             * If you want to view my strategy, just uncomment my function call
             * to proof_of_optimization in main. */
            cmp = strcmp(argv[i], INPUT_STR[j]);
            if (cmp == 0) {
                printf("%s\n", RESPONSE_STR[j]);
                is_found = 1;
                break;
            }
            else if (cmp < 0 && strlen(argv[i]) <= strlen(INPUT_STR[j])) {
                break;
                // Boom, optimization. cmp < 0 comes before function calls,
                // thus this should result in a net optimization since the
                // evaluation of "else if" would end before reaching strlen().
                // This will occur a vast majority of the time since it is rare
                // for a word with more letters to have a higher net ASCII value
                // than a word with less letters.
            }
        }
    }
    if (!is_found) {
        printf("%s\n", RESPONSE_STR[5]);
    }
    
    printf("--------\n\n");
}

