#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILE_NAME "test_matrix.dat"
#define uint32_t u_int32_t

int  arg_test(int argc, char **argv);
void load_data(int ***int_array, uint32_t *row, uint32_t *col);
void find_nth(int **int_array, uint32_t row, uint32_t col, int n, int *ret_arr);
void print_matrix(uint32_t row, uint32_t col, int **arr);
int find_nth_in_col(int **array, uint32_t row, uint32_t col, int n);

int main(int argc, char **argv)
{
    printf("---- Answer ----\n");
    
    int valid = arg_test(argc, argv);
    
    int nth = -3;
 
    if(valid) {
        int **int_array = NULL;
        uint32_t row = 0;
        uint32_t col = 0;
        
        load_data(&int_array, &row, &col);
        int *ret_array = (int*) malloc(sizeof(int) * col);

        // Insert your code here (1)
        int n = atoi(argv[1]);

        // ------------------------

        find_nth(int_array, row, col, n, ret_array);

        int i;
        for(i = 0; i < col; i++) {
            printf("%d ", ret_array[i]);
        }
        printf("\n");
        
    }

    printf("--------\n");
    
    return 0;
}


int arg_test(int argc, char **argv)
{
    int return_val = 0;
    if(argc < 2) {
        fprintf(stderr, "Error: no input entered\n");
        fprintf(stderr, "usage: %s <n>\n", argv[0]);
        fprintf(stderr, "\n");
    } else if(argc > 2) {
        fprintf(stderr, "Error: too many inputs\n");
        fprintf(stderr, "usage: %s <n>\n", argv[0]);
        fprintf(stderr, "\n");
    } else {
        return_val = 1;
    }
    return return_val;
}

void load_data(int ***int_array, uint32_t *row, uint32_t *col)
{
    FILE *fp = NULL;
    fp = fopen(FILE_NAME, "r");
    if(fp == NULL) {
        fprintf(stderr, "Error while loading the file\n");
        exit(EXIT_FAILURE);
    }

    int r = 0;
    int c = 0;
    fscanf(fp, "%d", &r);
    fscanf(fp, "%d", &c);

    int cnt = 0;
    int tmp = 0;
    while(fscanf(fp, "%d", &tmp) == 1) {
        cnt++;
    }
    fclose(fp);

    if(r * c == cnt) {
    } else {
        printf("Dimension and number of integers do not match: %d x %d = %d!\n",
               r, c, cnt);
        exit(0);
    }

    int **tmp_array = (int**) malloc(sizeof(int*) * r);
    
    int i;
    for(i = 0; i < r; i++) {
        tmp_array[i] = (int*) malloc(sizeof(int) * c);
    }

    fp = fopen(FILE_NAME, "r");
    if(fp == NULL) {
        fprintf(stderr, "Error while loading the file\n");
        exit(EXIT_FAILURE);
    }

    fscanf(fp, "%d", &tmp);
    fscanf(fp, "%d", &tmp);
 
    cnt = 0;
    tmp = 0;
    while (fscanf(fp, "%d", &tmp) == 1) {
        tmp_array[cnt / c][cnt % c] = tmp;
        cnt++;
    }
    fclose(fp);
    
    *int_array = tmp_array;
    *row = r;
    *col = c;
}

// Given n as input, find the nth largest value
// in each column of the matrix.
// If n is larger than the number of rows,
// return -1.
// Return -2 for any other errors.
// NOTE 1:
// The file used for grading will be different from
// the file given with the homework - it will have
// different sized matrix with different values.
void find_nth(int **int_array, uint32_t row, uint32_t col, int n, int *ret_arr)
{
   // Insert your code here (2)
    int i, j;
    if (n > col) {
        for (i = 0; i < col; ++i) {
            ret_arr[i] = -1;
        }
        return;
    }
    else if (n <= 0) {
        for (i = 0; i < col; ++i) {
            ret_arr[i] = -2;
        }
        return;
    }
    
    for (i = 0; i < row; ++i) {
        ret_arr[i] = find_nth_in_col(int_array, i, col, n);
    }

   // -------------------------
}

int find_nth_in_col(int **array, uint32_t row, uint32_t col, int n) {
    int i, j;
    
    // If only one column, return the only entry
    if (col == 1)
        return array[row][0];
    
    // Initialize lowest to arbitrary value, so first value in row
    int lowest = array[row][0];
    
    // Resolve lowest number in array without making new array
    for (j = 0; j < col; ++j) {
        if (array[row][j] < lowest)
            lowest = array[row][j];
    }

    // Initialize lowest difference, lowest with first if lowest != first
    // lowest with second otherwise.
    int lowest_diff, new_lowest;
    if (lowest == array[row][0]){
        lowest_diff = array[row][1] - lowest;
        new_lowest = array[row][1];
    }
    else {
        lowest_diff = array[row][0] - lowest;
        new_lowest = array[row][0];
    }
    
    /*
     * Given the limitations of the assignment, I had to do this in an obscure way.
     * So... I start with an arbitrary lowest difference (lowest_diff) between
     * either the lowest number and the first number, or the lowest number
     * and the second number, depending on if the lowest number was the first
     * number. Then I begin iterating through all the entries of the given row,
     * finding which one is the next lowest difference between the lowest
     * number and the entry. After iterating through all differences, it
     * keeps track of which one is the lowest and sets the next lowest number
     * equal to the index of the array that created that lowest difference. If
     * we have iterated through the row n times, we stop as we have found the
     * nth lowest difference implying the nth lowest number.
     * I set limitations such as (compare > 0) because since we're not allowed
     * to keep copies of the array, thus we'll have to do a lot of extra
     * iterations of comparisons, comparing numbers that are less
     * than the new lowest. At the very end, we will have found the n'th lowest
     * number, and we return it for storing in the return array.
     */
    // printf("\n\nENTERING NEW ROW\n\n");
    i = 1;
    int compare;
    int highest_diff = 0;
    // printf("Entering in with a lowest_diff of %d.\n", lowest_diff);
    while (i != n) {
        for (j = 0; j < col; ++j){
            compare = array[row][j] - lowest;
            // printf("Comparing %d with %d, got %d.\n", array[row][j], lowest, compare);
            if (compare < lowest_diff && compare > 0){
                // printf("This is less than %d! Setting lowest_diff to %d.\n", lowest_diff, compare);
                lowest_diff = compare;
                new_lowest = array[row][j];
            }
            if (compare > highest_diff) {
                highest_diff = compare;
            }
        }
        // printf("Setting new lowest to %d.\n", new_lowest);
        lowest = new_lowest;
        lowest_diff = highest_diff;
        ++i;
    }
    return lowest;
}
