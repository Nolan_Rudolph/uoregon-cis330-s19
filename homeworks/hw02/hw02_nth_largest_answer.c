#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILE_NAME "test.dat"
#define uint32_t u_int32_t

int  arg_test(int argc, char **argv);
void load_data(int **int_array, uint32_t *array_size);
int  find_nth(int *int_array, uint32_t array_size, int n);
int compare_func(const void *, const void *);

int main(int argc, char **argv)
{
    int valid = arg_test(argc, argv);
    
    int nth = -1;
 
    if(valid) {
        int *int_array = NULL;
        uint32_t array_size = 0;
        load_data(&int_array, &array_size);

        // Insert your code here (1)
        int n = atoi(argv[1]);
        nth = find_nth(int_array, array_size, n);
        
        printf("---- Answer ----\n");
        
        if(nth >= 0) {
            printf("The nth value is %d\n", int_array[nth]);
        } else {
            printf("Please input a number between 1 and %d\n", array_size);
        }
        
        printf("--------\n");
        // ------------------------
    }
    
    return 0;
}



int arg_test(int argc, char **argv)
{
    int return_val = 0;
    if(argc < 2) {
        fprintf(stderr, "Error: no input entered\n");
        fprintf(stderr, "usage: %s <n>\n", argv[0]);
        fprintf(stderr, "\n");
    } else if(argc > 2) {
        fprintf(stderr, "Error: too many inputs\n");
        fprintf(stderr, "usage: %s <n>\n", argv[0]);
        fprintf(stderr, "\n");
    } else {
        return_val = 1;
    }
    return return_val;
}

void load_data(int **int_array, uint32_t *array_size)
{
    FILE *fp = NULL;
    fp = fopen(FILE_NAME, "r");
    if(fp == NULL) {
        fprintf(stderr, "Error while loading the file\n");
        exit(EXIT_FAILURE);
    }

    int cnt = 0;
    int tmp = 0;
    while (fscanf(fp, "%d", &tmp) == 1) {
        cnt++;
    }
    fclose(fp);

    int *tmp_array = (int*) malloc(sizeof(int) * cnt);
    fp = fopen(FILE_NAME, "r");
    if(fp == NULL) {
        fprintf(stderr, "Error while loading the file\n");
        exit(EXIT_FAILURE);
    }
    cnt = 0;
    tmp = 0;
    while (fscanf(fp, "%d", &tmp) == 1) {
        tmp_array[cnt] = tmp;
        cnt++;
    }
    fclose(fp);
    
    *int_array = tmp_array;
    *array_size = cnt;
}

int compare_func(const void *num1, const void *num2) {
    int *tmp1 = (int *)num1;
    int *tmp2 = (int *)num2;
    
    return *tmp1 - *tmp2;
}

// Given n as input, find the nth largest value
// in the list of integers loaded from the file.
// If n is larger than the number of integers,
// return -1.
// Return -2 for any other errors.
// NOTE 1:
// The file used for grading will be different from
// the file given with the homework - it will have
// different number of integers and different integer
// values
int find_nth(int *int_array, uint32_t array_size, int n)
{
    if (n > array_size) return -1;
    else if (n <= 0) return -2;
    
    // Insert your code here (2)
    int tmp_array[array_size];
    
    // Transfer data to new array to be sorted
    int i;
    for (i = 0; i < array_size; ++i) {
        tmp_array[i] = int_array[i];
    }
    
    // Sort new array (qsort is in stdlib :D)
    qsort(tmp_array, sizeof(tmp_array)/sizeof(*tmp_array), sizeof(*tmp_array), compare_func);
    
    // Grab the nth largest term
    int largest = tmp_array[n - 1];
    
    // Grab the real index from the original list
    for (i = 0; i < array_size; ++i) {
        if (largest == int_array[i])
            return i;
    }
    
    printf("Could not find n'th largest term.\n");
    
    return -3;
    // ------------------------

}

void print_array(uint32_t size, int *arr)
{
    printf("---- Print Array ----\n");
    printf("This file has %d elements\n", size);
    printf("#\tValue\n");
    int i;
    for(i = 0; i < size; i++) {
        printf("%d\t%d\n", i, arr[i]);
    }
   printf("--------\n\n");
}

