#include "cipher.h"

#define UPPER_CASE(r) ((r) - ('a' - 'A'))

struct Cipher::CipherCheshire {
    string cipherText;
};

Cipher::Cipher()
{
    smile = new CipherCheshire;
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
}
Cipher::Cipher(string in)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
}
string Cipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;               // Spaces correspond to spaces in cipherText
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;
}

string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting..." << endl;
    for(unsigned int i = 0; i < enc.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(enc[i] == ' ') {
            pos = 0;
        } else if(enc[i] >= 'a') {
            pos = enc[i] - 'a' + 1; // Simply tacking on a one will compensate for ' '
        } else {
            pos = enc[i] - 'A' + 1; // Again, so simple solution for decryption
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;
}




struct CaesarCipher::CaesarCipherCheshire : CipherCheshire {
     int rot;
};

CaesarCipher::CaesarCipher()
{
    
    smile = new CipherCheshire;  // CaesarCiper inherits CipherCheshire inherits Cipher
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz "; // Allows us to adopt smile
    CaesarSmile = new CaesarCipherCheshire;  // CaesarCipher has new member CaesarSmile
}

CaesarCipher::CaesarCipher(string in, int rot)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile -> rot = rot;  // In this constructor, we can define now rot
}

string CaesarCipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        int rot = CaesarSmile -> rot;
        if(raw[i] == ' ') {
            if (rot == 0)  // I found my method is flawed when only spaces are inputted
                pos = 26;
            else
                pos = rot - 1;  // Otherwise, shift by rot (-1 for space)
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a' + rot;  // Again, use the rot shift
        } else {
            pos = raw[i] - 'A' + rot;  // Again
            upper = 1;
        }
        if (pos > 26) {
            pos %= 26 + 1;  // This allows us to make a full loop of our alphabet
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;

}

string CaesarCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    for(unsigned int i = 0; i < enc.size(); i++) {
        unsigned int pos;
        bool upper = false;
        int rot = CaesarSmile -> rot;
        if(enc[i] == ' ') {
            if (rot == 0)
                pos = 0;
            else
                pos = 26 - rot + 1;
        } else if(enc[i] >= 'a') {
            pos = enc[i] - 'a' - rot + 1; // As we've seen in the first decrypt,
        } else {
            pos = enc[i] - 'A' - rot + 1; // simply adding one will reverse affect
            upper = 1;
        }
        if ((int)pos < 0) {
            pos = 26 + pos + 1; // Again we want capabilities of string looping
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }

    cout << "Done" << endl;

    return retStr;
}


