//
// Implementation of methods for classes Response, AngryResponse, HappyResponse
//
#include <iostream>
#include <string>
#include <algorithm>

#include "response.hh"

using namespace std;

//
// Implementation of Word methods
//
// Don't worry too hard about these implementations.
// If you'd like to learn more about STL see: https://www.geeksforgeeks.org/the-c-standard-template-library-stl/
//
string Word::upper()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::toupper);
  return result;
}

string Word::lower()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::tolower);
  return result;
}

//
// Implementation of Response methods
//
bool Response::checkAndRespond(const string &inWord, ostream &toWhere)
{
    Word word = this->keyword;
    
    if (inWord == word.upper()) {
        this->respond(toWhere);
        return true;
    }
    else {
        return false;
    }
}

void Response::respond(ostream &toWhere)
{
    toWhere << this->response << endl;
}


//
// Implementation of AngryReponse methods
//

void AngryResponse::respond(ostream &toWhere) {
    toWhere << this->response << "  D:<" << endl;
}
//
// Implementation of HappyResponse methods
//
void HappyResponse::respond(ostream &toWhere) {
    toWhere << this->response << "  :D" << endl;
}